#pragma once

#include <vector>
using namespace std;

struct Pair 
{
	int start;
	int stop;
};

class WayPair
{
	public:
		WayPair(int id);
		~WayPair();
		void InsertPair(int start, int stop);
		int GetLength();
		int GetId();

	private:
		int id;
		vector<Pair> pairs;
};


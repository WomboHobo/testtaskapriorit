#pragma once
#include "WayPair.h"
namespace AprioritTestTask 
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;


	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
		}

	protected:
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}

#pragma region Components

	private: System::Windows::Forms::Button^  data_in;
	protected: 
	private: System::Windows::Forms::Button^  data_out;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  text_output;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  prog_output;

	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;

	private:
		System::ComponentModel::Container ^components;
#pragma endregion

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->data_in = (gcnew System::Windows::Forms::Button());
			this->data_out = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->text_output = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->prog_output = (gcnew System::Windows::Forms::Label());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->SuspendLayout();
			// 
			// data_in
			// 
			this->data_in->Location = System::Drawing::Point(12, 12);
			this->data_in->Name = L"data_in";
			this->data_in->Size = System::Drawing::Size(158, 23);
			this->data_in->TabIndex = 0;
			this->data_in->Text = L"������� ������";
			this->data_in->UseVisualStyleBackColor = true;
			this->data_in->Click += gcnew System::EventHandler(this, &Form1::data_in_Click);
			// 
			// data_out
			// 
			this->data_out->Location = System::Drawing::Point(12, 41);
			this->data_out->Name = L"data_out";
			this->data_out->Size = System::Drawing::Size(158, 23);
			this->data_out->TabIndex = 1;
			this->data_out->Text = L"���������";
			this->data_out->UseVisualStyleBackColor = true;
			this->data_out->Click += gcnew System::EventHandler(this, &Form1::data_out_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 113);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(157, 17);
			this->label1->TabIndex = 2;
			this->label1->Text = L"��������� (�� ������):";
			// 
			// text_output
			// 
			this->text_output->AutoSize = true;
			this->text_output->Location = System::Drawing::Point(175, 113);
			this->text_output->Name = L"text_output";
			this->text_output->Size = System::Drawing::Size(0, 17);
			this->text_output->TabIndex = 3;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(9, 87);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(157, 17);
			this->label2->TabIndex = 4;
			this->label2->Text = L"��������� ���������:";
			// 
			// prog_output
			// 
			this->prog_output->AutoSize = true;
			this->prog_output->Location = System::Drawing::Point(172, 87);
			this->prog_output->Name = L"prog_output";
			this->prog_output->Size = System::Drawing::Size(0, 17);
			this->prog_output->TabIndex = 5;
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(418, 213);
			this->Controls->Add(this->prog_output);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->text_output);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->data_out);
			this->Controls->Add(this->data_in);
			this->Name = L"Form1";
			this->Text = L"������� �����";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private:
		//int calculateres(String^ filename);

	private: System::Void data_in_Click(System::Object^  sender, System::EventArgs^  e) 
			 {
				 openFileDialog1->Multiselect = false;
				 openFileDialog1->Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
				 openFileDialog1->FilterIndex = 1;
				 openFileDialog1->RestoreDirectory = true;
				 
				//Load data from file
				 if(openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)  
				 {
					String^ fileName = openFileDialog1->InitialDirectory + openFileDialog1->FileName;
					try   
					{  
						StreamReader^ din = File::OpenText(fileName);  

						String^ str;
						bool isFirst = true;
						int n_m[3];
						int r_line[3];
						vector< WayPair > data;
						while ((str = din->ReadLine()) != nullptr)   
						{  
							array<String^> ^StringArray = str->Split(' ');
							int count = 0;
							for each(String^ temp in StringArray)
							{
								if(isFirst)
								{
									n_m[count] = Convert::ToInt32(temp);
								}
								else
								{
									r_line[count] = Convert::ToInt32(temp);
								}
								count++;
							}
							
							if(!isFirst)
							{
								bool isNew = true;
								for(int i=0; i<data.size(); i++){
									if (data.at(i).GetId() == r_line[0])
									{
										isNew = false;
										data.at(i).InsertPair(r_line[1], r_line[2]);
									}
								}
								if(isNew){
									WayPair tmp = WayPair(r_line[0]);
									tmp.InsertPair(r_line[1], r_line[2]);
									data.push_back(tmp);
								}
							}
							double chk = Math::Pow(10,9);
							if ((n_m[0] < 1) || (Convert::ToDouble(n_m[0]) > chk) || (n_m[1] < 1) || (Convert::ToDouble(n_m[1]) > chk) || (n_m[2] < 0) || (n_m[2] > 1000))
							{
								MessageBox::Show("�� ���������� ���������", "������",MessageBoxButtons::OK, MessageBoxIcon::Exclamation);
								din->Close();
								break;
							}
							isFirst = false;
						}  
						UInt64 counter = n_m[0];
						counter *= n_m[1];
						for each(WayPair way_selected in data) 
						{
							counter -= way_selected.GetLength();
						}

						prog_output->Text = Convert::ToString(counter);
					}  
					catch (Exception^ e)  
					{  
						if (dynamic_cast<FileNotFoundException^>(e))  
							Console::WriteLine("file '{0}' not found", fileName);  
						else  
							Console::WriteLine("problem reading file '{0}'", fileName);  
					} 
				 }  
			 }
	private: 
		System::Void data_out_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			openFileDialog1->Multiselect = false;
			openFileDialog1->Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
			openFileDialog1->FilterIndex = 1;
			openFileDialog1->RestoreDirectory = true;

			//Load data from file
			if(openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)  
			{
				String^ fileName = openFileDialog1->InitialDirectory + openFileDialog1->FileName;
				try   
				{  
					StreamReader^ din = File::OpenText(fileName);  

					String^ str;
					String^ str2;
					str = din->ReadToEnd();
					text_output->Text = str;
					str2 = prog_output->Text;
					if (str == str2)
					{
						text_output->BackColor = System::Drawing::Color::LightGreen;
						prog_output->BackColor = System::Drawing::Color::LightGreen;
					}
					else
					{
						text_output->BackColor = System::Drawing::Color::LightPink;
						prog_output->BackColor = System::Drawing::Color::LightPink;
					}
				}  
				catch (Exception^ e)  
				{  
					if (dynamic_cast<FileNotFoundException^>(e))  
						Console::WriteLine("file '{0}' not found", fileName);  
					else  
						Console::WriteLine("problem reading file '{0}'", fileName);  
				}
			}
		}
};
}


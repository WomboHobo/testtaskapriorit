#include "StdAfx.h"
#include "WayPair.h"


WayPair::WayPair(int id)
{
	this->id = id;
}

WayPair::~WayPair()
{
	this->pairs.clear();
}

int WayPair::GetId()
{
	return this->id;
}

int WayPair::GetLength()
{
	int length = 0;
	for each(Pair pair in this->pairs) 
	{
		length += pair.stop - pair.start + 1;
	}
	return length;
}

void WayPair::InsertPair(int start, int stop)
{
	bool is_new = true;
	for(int i =0; i<pairs.size(); i++)
	{
		if(start >= pairs.at(i).start && start <= pairs.at(i).stop)
		{
			if(stop > pairs.at(i).stop)
			{
				pairs.at(i).stop = stop;
				is_new = false;
				break;
			}
			else
			{
				is_new = false;
				break;
			}
		}
		else
		{
			if(stop >= pairs.at(i).start && stop <= pairs.at(i).stop)
			{
				pairs.at(i).start = start;
				is_new = false;
				break;
			}
		}
	}
	if(is_new == true)
	{
		Pair tmp;
		tmp.start = start;
		tmp.stop = stop;
		this->pairs.push_back(tmp);
	}
}